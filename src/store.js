import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
        apikey: '',
        selectedFilm: {}
  },
  mutations: {
    setApiKey(state, key) {
        state.apikey = key
    },

    setSelectedFilm(state, film) {
        state.selectedFilm = film
    }
  },
  actions: {

  }
})
