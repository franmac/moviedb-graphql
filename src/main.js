import Vue from 'vue'
import App from './App.vue'
import store from './store'
import Buefy from 'buefy'

import './scss/main.scss'

import axios from 'axios'

import { createProvider } from './vue-apollo'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar, faAlignLeft, faListUl, faUsers, faSpinner } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faStar, faAlignLeft, faListUl, faUsers, faSpinner)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.prototype.$http = axios

Vue.config.productionTip = false
Vue.use(Buefy)

new Vue({
  store,
  provide: createProvider().provide(),
  render: h => h(App)
}).$mount('#app')