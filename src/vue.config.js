module.exports = {
  chainWebpack: config => {
    // GraphQL Loader
    config.module
      .rule('graphql')
      .test(/\.(graphql|gql)$/)
      .use('graphql-tag/loader')
        .loader('graphql-tag/loader')
        .end()
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: `@import "~@/scss/_colors.scss";`
      }
    }
  }
}