import gql from 'graphql-tag'

export const GET_MOVIES = gql`
query ($query: String!) {
  movies(query: $query) {
		id
		title
		overview
		vote_average
		popularity
  }
}
`

export const GET_MOVIES_BY_GENRE = gql`
query ($id: Int!) {
	movies_by_genre(id: $id) {
		id
		title
		overview
		vote_average
		popularity
	}
}
`