# Movie DB
Buscador de películas utilizando la API de MovieDB y GraphQL

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Acceso
```
Acceso desde http://localhost:8080
```

# moviedb apollo-server

## Project setup
```
cd movie-database-graphql
npm install
```

### Run server
```
npm run start
```